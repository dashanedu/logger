<?php

return [
    'aliyun-sls' => [
        'endpoint'      => env('LOGHUB_ENDPOINT', ''),
        'accessKeyId'   => env('LOGHUB_ACCESS_KEY_ID', ''),
        'accessKey'     => env('LOGHUB_ACCESS_KEY', ''),
        'logProject'    => env('LOGHUB_PROJECT', ''),
        'logStore'      => env('LOGHUB_STORE', ''),
        'logTopic'      => env('LOGHUB_TOPIC', '')
    ],
];
