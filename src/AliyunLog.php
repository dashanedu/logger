<?php

namespace Dashan\Logger;

use Dashan\Logger\Classes\AliyunLogClient;
use Dashan\Logger\Classes\LoggerFactory;

class AliyunLog
{
    protected static $logger;

    protected static function initInstance()
    {
        if (!self::$logger) {
            $config = config('logger.aliyun-sls');
            $client = new AliyunLogClient($config['endpoint'], $config['accessKeyId'], $config['accessKey']);
            self::$logger = LoggerFactory::getLogger($client, $config['logProject'], $config['logStore'], $config['logTopic']);
        }
    }

    public static function info($collect_no, $message)
    {
        self::initInstance();
        self::$logger->info($collect_no, $message);
    }

    public static function warn($collect_no, $message)
    {
        self::initInstance();
        self::$logger->warn($collect_no, $message);
    }

    public static function error($collect_no, $message)
    {
        self::initInstance();
        self::$logger->error($collect_no, $message);
    }

    public static function debug($collect_no, $message)
    {
        self::initInstance();
        self::$logger->debug($collect_no, $message);
    }

    public static function infoArray($collect_no, $messages)
    {
        self::initInstance();
        self::$logger->infoArray($collect_no, $messages);
    }

    public static function warnArray($collect_no, $messages)
    {
        self::initInstance();
        self::$logger->warnArray($collect_no, $messages);
    }

    public static function errorArray($collect_no, $messages)
    {
        self::initInstance();
        self::$logger->errorArray($collect_no, $messages);
    }

    public static function debugArray($collect_no, $messages)
    {
        self::initInstance();
        self::$logger->debugArray($collect_no, $messages);
    }
}